# Compilation avec le script Maven

La compilation des projets **Spring Boot** est faite en utilisant le script Maven (script `mvnw`) fournit par l'initialisation standard d'un projet Spring Boot.

```bash
cd path.to.the.project.folder
./mvnw clean package # l'exécutable .jar se trouvera dans le répertoire ./target/

# ou bien vous pouvez compiler et faire le lancement du programme directement
# en une seule commande
./mvnw spring-boot:run
```

Pour en savoir plus sur l'utilisation de ce script, ses options etc. : [Maven wrapper](https://www.codeflow.site/fr/article/maven-wrapper).

Les requêtes documentées ci-dessous peuvent être testées en utilisant `Curl`, `Postman` ou autre client HTTP. 

Toutes les requêtes sont sécurisées par des clés [Jason Web Token (JWT)](https://jwt.io/). La classe utilisée pour gérer les `JWT` : [https://github.com/auth0/java-jwt](https://github.com/auth0/java-jwt).

Certaines requêtes nécessitent qu'un utilisateur soit authentifié. Dans ce cas, le JWT à utiliser sera celui fourni après une requête de `login`. 
Pour les autres requêtes, vous pouvez utiliser cette clé : `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlcyI6bnVsbCwiaXNzIjoiQllUUEFZTUVOVF9LTk9XTl9DTElFTlQiLCJpZCI6bnVsbH0.gBhQArcs6WHQfTrqxBkyw7-ue7WU2a1DHyjdwIND_nU`

La clé JWT données ci-haut sera appelée `STATIC_TOKEN` dans le reste de ce document.


# Gestion des utilisateurs `<springboot.auth.bytpayment.com>`

L'intégralité de la gestion des utilisateurs est faire par ce service. Les requêtes possibles sont exposées via des URLs (Routes) qui correspondent à des méthodes précises du contrôleur de ce service : `UserController.java`.

[Classe User.java](https://gitlab.com/buy-your-ticket/springboot.auth.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/auth/Model/User.java)

#### CREATE ou REGISTER

```
PUT
* Authorization > Bearer [STATIC_TOKEN]
https://<AUTH_SERVER_ADDRESS>/user/register
```

Un utilisateur sera créé en base de données s'il est valide. 
Voici le minimum des champs requis pour qu'un utilisateur soit valide :
```json
{
  "login": "userlogin",
  "password": "userpassword",
  "lastname": "userlastname",
  "firstname": "userfirstname"
}
```

Cette requête retourne l'utilisateur créé en cas de succès.

#### LOGIN

```
POST
* Authorization > Bearer [STATIC_TOKEN]
https://<AUTH_SERVER_ADDRESS>/user/login
```
Paramètres de la requête :
```json
{
  "login": "userlogin",
  "password": "userpassword"
}
```
En cas de succès, l'objet qui sera retourné comporte un champ `jwt`. Ce token encodé en Base64 comporte des informations sur les ROLES de l'utilisateur qui vient de se connecter.

#### READ

Lecture d'un utilisateur dont l'identifiant correspond à celui passé en URL :
```
GET
* Authorization > Bearer Token
https://<AUTH_SERVER_ADDRESS>/user/read/{id}
```

#### DELETE

Suppression d'un utilisateur dont l'identifiant correspond à celui passé en URL :
```
DELETE
* Authorization > Bearer Token
https://<AUTH_SERVER_ADDRESS>/user/delete/{id}
```

#### READ_ALL

Lecture de tous les utilisateurs avec des possibilités de filtrer par `login` (startsWithIgnoreCase), `lastname` (startsWithIgnoreCase), `firstname` (startsWithIgnoreCase), `birthday` (eq) et `email` (startsWithIgnoreCase).
On peut y ajouter aussi un objet comportant les paramètres de pagination : 
- limit
- offset
- sortBy (par défaut : `"lastname"`)

Tous ces filtres sont décrits dans la classe [RequestReadAll.java](https://gitlab.com/buy-your-ticket/springboot.auth.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/auth/Helpers/RequestReadAll.java).

```
POST
* Authorization > Bearer Token
https://<AUTH_SERVER_ADDRESS>/user/readAll
```

#### UPDATE

```
POST
* Authorization > Bearer Token
https://<AUTH_SERVER_ADDRESS>/user/update
```

Mise à jour des champs d'un utilisateur. Seuls les champs renseignés dans le corps de la requête seront modifiés.
Il est **obligatoire** de renseigner l'identifiant de l'utilisateur pour lequel l'on veut actualiser les informations.

Exemple :
```json
{
  "id": "stringUserId",
  "lastname": "newlastname",
  "firstname": "newfirstname"
}
```
Une réponse JWT sera retourné au succès de cette mise à jour (voir JWT lors du `LOGIN`).

#### READ_BY_TELEPHONE

Recherche de tous les utilisateurs ayant un numéro de téléphone (renseigné dans l'URL). On peut ajouter les paramètres de pagination (voir `READ_ALL`).
```
GET
* Authorization > Bearer Token
https://<AUTH_SERVER_ADDRESS>/user/readByTelephone/{telephone}
```

#### READ_BY_ROLE

Recherche de tous les utiliateurs ayant comme droit un `ROLE` donné dans l'URL. On peut ajouter les paramètres de pagination (voir `READ_ALL`).
```
GET
* Authorization > Bearer Token
https://<AUTH_SERVER_ADDRESS>/user/readByRole/{role}
```

#### READ_BY_CODE

Recherche d'un utilisateur dont le code correspond à celui passé dans l'URL.
```
GET
* Authorization > Bearer Token
https://<AUTH_SERVER_ADDRESS>/user/readByCode/{code}
```

# Gestion des services `<springboot.services.bytpayment.com>`

## Événements

Le contrôleur de ce service est [EventController.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Controller/EventController.java) et la structure d'un événement est décrite par la classe [Event.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Model/Event.java).

#### CREATE

La création d'un événement doit se faire par un utilisateur authentifié.
```
PUT
* Authorization > Bearer Token
https://<SERVICES_SERVER_ADDRESS>/event/create
```
Une vérification de la validité d'un événement est faite avant sa création.
Exemple d'un événement valide :
```json
{
  "name": "eventname",
  "date": "2020-05-30T18:30:00",
  "prices": [
    {
      "type": "pricetype",
      "values": [
        {
          "currency": "BIF",
          "value": 5000.0
        }
      ],
      "totalPlaces": 100
    }
  ]
}
```
Structure des classes :
- [LocalCurrency.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Model/LocalCurrency.java)
- [Price.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Model/Price.java)

L'événement créé sera retourné au succès de la requête.

#### READ

Lecture d'un événement dont l'identifiant correspond à celui passé en URL :
```
GET
* Authorization > Bearer Token ou [STATIC_TOKEN]
https://<SERVICES_SERVER_ADDRESS>/event/read/{id}
```

#### READ_BY_CODE

Lecture d'un événement dont le code correspond à celui passé dans l'URL.
```
GET
* Authorization > Bearer Token ou [STATIC_TOKEN]
https://<SERVICES_SERVER_ADDRESS>/event/readByCode/{code}
```

#### READ_BY_CODE_SIMPLE

Lecture d'un événement par son code sans retourner des informations lourdes comme la `photo`.
```
GET
* Authorization > Bearer Token ou [STATIC_TOKEN]
https://<SERVICES_SERVER_ADDRESS>/event/readByCodeSimple/{code}
```

#### DELETE

Suppression d'un événement dont l'identifiant correspond à celui passé dans l'URL.
```
DELETE
* Authorization > Bearer Token
https://<SERVICES_SERVER_ADDRESS>/event/delete/{id}
```

#### UPDATE

Mise à jour d'un événement. Ici l'objet qui sera passé en paramètres de la requête correspondra à ce qui sera attendu comme résultat après la mise à jour.
Cependant, certains champs resteront inchangés : `code`, `seller` et `lockRevision`.
```
POST
* Authorization > Bearer Token
https://<SERVICES_SERVER_ADDRESS>/event/update
```

#### READ_ALL

Lecture de tous les événements avec des possibilités de filtrer par :
- `name` (startsWithIgnoreCase)
- `tags` (contains)
- `date` (goe)
- `seller` (eq)
- objet `venue` : `city`(equalsIgnoreCase), `country` (equalsIgnoreCase), `number` (startsWithIgnoreCase), `road` (startsWithIgnoreCase), `details` (contains)

On peut y ajouter aussi des paramètres de pagination : 
- limit
- offset
- sortBy (par défaut : `"name"`)

Tous ces filtres sont décrits dans la classe [RequestEventReadAll.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Helpers/RequestEventReadAll.java).

```
POST
* Authorization > Bearer Token ou [STATIC_TOKEN]
https://<SERVICES_SERVER_ADDRESS>/event/readAll
```

## Codes

Ce sont des codes générés et qui sont utilisés pour la création des tickets.
- Contrôleur [CodeController.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Controller/CodeController.java)
- Classe [Code.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Model/Code.java)

#### CREATE

Un code a une valeur monétaire bien précise. Le corps de la requête de création d'un code aura la structure décrite par la classe [LocalCurrency.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Model/LocalCurrency.java).

```
PUT
* Authorization > Bearer Token
https://<SERVICES_SERVER_ADDRESS>/code/create
```
Exemple de corps de la requête :
```json
{
  "currency": "BIF",
  "value": 5000
}
```

Les monnaies prises en charge : [Currency.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Model/Currency.java).

#### READ

Lecture d'un Code par son identifiant.
```
GET
* Authorization > Bearer Token ou [STATIC_TOKEN]
https://<SERVICES_SERVER_ADDRESS>/code/read/{id}
```

#### READ_BY_CODE

Lecture d'un Code dont la valeur du champ `code` est passé à l'URL.
```
GET
* Authorization > Bearer Token ou [STATIC_TOKEN]
https://<SERVICES_SERVER_ADDRESS>/code/readByCode/{code}
```

#### READ_ALL

Lecture de tous les codes avec possibilité de filtrer par :
- `createdBy` (eq)
- `createdAt` (goe)
- `usedBy` (eq)
- `usedAd` (goe)
- objet `value` : `currency` (eq) et `value` (goe)

On peut y ajouter aussi des paramètres de pagination : 
- limit
- offset
- sortBy (par défaut : `"code"`)

Tous ces filtres sont décrits dans la classe [RequestCodeReadAll.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Helpers/RequestCodeReadAll.java).

```
POST
* Authorization > Bearer Token ou [STATIC_TOKEN]
https://<SERVICES_SERVER_ADDRESS>/code/readAll
```

## Tickets

Gestion des tickets pour des événements.

- Contrôleur de la gestion des tickets [TicketController.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Controller/TicketController.java)
- Classe [Ticket.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Model/Ticket.java)

#### CREATE_BY_CODE

Création d'un ticket pour un événement avec un code généré. Cette opération correspond à l'achat d'un ticket.

```
PUT
* Authorization > Bearer Token
https://<SERVICES_SERVER_ADDRESS>/ticket/createByCode
```

Paramètres dans le corps de la requête [RequestCreateTicketByCode](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Helpers/RequestCreateTicketByCode.java) :
```json
{
  "code": "code", // valeur du champ 'code' d'un Code généré
  "priceId": "priceId", 
  "eventCode": "eventCode" 
}
```

#### READ

Lecture d'un ticket par son identifiant.

```
GET
* Authorization > Bearer Token ou [STATIC_TOKEN]
https://<SERVICES_SERVER_ADDRESS>/ticket/read/{id}
```

#### READ_ALL

Lecture de tous les tickets avec possibilité de filtrer par :

- `eventCode` (eq)
- `seller` (eq)
- `user` (eq)
- objet `price` : `currency` (eq) et `value` (goe)
- `createdAt` (goe)
- `valide` (eq)
- `valideStatusChangedAt` (goe)

On peut y ajouter aussi des paramètres de pagination : 
- limit
- offset
- sortBy (par défaut : `"createdAt"`)

Tous ces filtres sont décrits dans la classe [RequestTicketReadAll.java](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/services/Helpers/RequestTicketReadAll.java).

```
POST
* Authorization > Bearer Token ou [STATIC_TOKEN]
https://<SERVICES_SERVER_ADDRESS>/ticket/readAll
```

#### VALIDATION

Validation d'un ticket à partir de son numéro.
```
GET
* Authorization > Bearer Token
https://<SERVICES_SERVER_ADDRESS>/ticket/validation/{ticketNumber}
```

#### PAUSE

Mise en pause d'un ticket déjà acheté.
```
GET
* Authorization > Bearer Token
https://<SERVICES_SERVER_ADDRESS>/ticket/pause/{ticketNumber}
```

#### TICKETS_PER_WEEK

Liste des totaux - tickets créés sur une période : (par semaine pour ce cas)
```
GET
* Authorization > Bearer Token ou [STATIC_TOKEN]
https://<SERVICES_SERVER_ADDRESS>/ticket/ticketsPerWeek/{eventCode}
```