# Configurations sur un serveur de déploiement (Ubuntu Server)

## Création d'un utilisateur Linux

Source : https://linuxize.com/post/how-to-create-a-sudo-user-on-ubuntu/

Démarche à suivre pour la création d'un utilisateur non-root ayant un accès `sudo`: 

1. Se connecter au serveur
```bash
ssh root@ip_serveur
```
2. Création d'un nouvel utilisateur
```bash
adduser nomutilisateur
```
3. Accès `sudo` pour l'utilisateur créé
```bash
usermod -aG sudo nomutilisateur
```
4. Tester
```bash
su - nomutilisateur
sudo whoami
```
Une fois la création du nouvel utilisateur effectuée, on peut se connecter au serveur en utilisant son **_nomutilisateur_**
```bash
ssh nomutilisateur@ip_serveur
```

5. Supprimer un utilisateur Linux
```bash
# Passer à l'utilisateur root
sudo su -

# Suppression d'un utilisateur
userdel nomutilisateur

# Facultatif : On peut également supprimer le répertoire d'accueil de l'utilisateur que l'on veut supprimer et la mise en attente mail à l'aide de l'indicateur -r
userdel -r nomutilisateur
```

## Configuration Timezone du serveur

Heure sur le serveur à l'heure locale française + Langue en Français

```bash
sudo timedatectl set-timezone Europe/Paris
locale
locale -a
sudo locale-gen fr_FR.UTF-8
sudo update-locale LANG=fr_FR.UTF-8
locale
sudo reboot
```

## Installation de Docker

Installation de Docker sur un linux ubuntu : [Documentation officielle - Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

```bash
# Désinstallation des anciennes versions
sudo apt-get remove docker docker-engine docker.io containerd runc # le contenu de /var/lib/docker n'est pas supprimé

# Préparation du repository

sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Pour les architectures x86_64 / amd64
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Installation Docker Engine - Community
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

# Tester l'installation
sudo docker run hello-world

# Pour la désinstallation + suppression de /var/lib/docker
sudo apt-get purge docker-ce
sudo rm -rf /var/lib/docker
```

## Utilisation de Docker avec un utilisateur non-root

[Documentation officielle - Docker](https://docs.docker.com/install/linux/linux-postinstall/)

```bash
# Création d'un group docker
sudo groupadd docker

# Ajout de l'utilisateur courant dans ce group
sudo usermod -aG docker $USER

# Faire un redémarrage pour rendre ces modifications effectives
# ou bien, sur Linux, faire : 
newgrp docker

# Vérification sans la commande sudo
docker run hello-world
```

## Configuration du démarrage Docker au moment du Boot
[Documentation officielle - Docker](https://docs.docker.com/install/linux/linux-postinstall/)
```bash
sudo systemctl enable docker

# et pour désactiver le démarrage au boot
sudo systemctl disable docker
```

## Quelques exemples d'utilisation de Docker

### Création d'une Image

- [Documenation officielle - Docker](https://docs.docker.com/get-started/part2/) pour l'écriture d'un **Dockerfile** ;
- Build d'une image à partir d'un **Dockerfile**
```bash
docker image build -t <nomimage>:<tag> <répertoire où se trouve le dockerfile>

# Exemple :
# docker image build -t bulletinboard:1.0 .
# Le docker file se trouve dans le répertoire courant
```
- Démarrer un conteneur
```bash
# Exemple
docker run -p 8000:8080 -d --name bb bulletinboard:1.0

# -p : à partir du port 8000 de la machine, on peut accéder au port 8080 du conteneur
# -d : lancement en mode détaché
# --name : nom qui sera associé à mon conteneur

# Pour d'autres options : 
# docker run --help
```
- Suppression d'un conteneur
```bash
docker rm -f <nomduconteneur> # nom attribué avec --name ou bien tout simplement d'identifiant du conteneur
```

### Exemple de Dockerfile pour une application Spring Boot

On peut retrouver beaucoup plus d'informations sur le guide [Spring Boot](https://spring.io/guides/gs/spring-boot-docker/).
```Dockerfile
FROM openjdk:8-jdk-alpine
WORKDIR /usr/src/app
COPY <jar_file> .
CMD [ "java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "jar_file" ]
EXPOSE 8080
```

### Conteneur Docker pour MongoDB - Base de données

On utilisera ici une image [officielle **MongoDB**](https://hub.docker.com/_/mongo?tab=description&page=1)

Il faut dans un premier temps s'assurer que le conteneur MongoBD se trouve dans un même network que les autres conteneurs intéragissant avec lui.

```bash
docker network ls # lister les réseaux disponibles - docker

# création du réseau :
docker network create -d bridge monreseau # -d : driver utilisé, voir dans la liste des réseaux

# Pour le conteneur de la base de données, il faut utiliser un Volume pour la persistence des données.

# Deux possibilités :
# 1.
docker volume create mesdata # stockage dans /var/lib/docker/volumes/mesdata
docker run -v mesdata:/data/db --name dbcontainer --network "monreseau" mongo:4.2.1-bionic

# 2.
# Au moment du lancement du conteneur, spécifier le volume en donnant un chemin sur la machine hôte
docker run --name dbcontainer -v /path/to/host/storage:/data/db --network "monreseau" mongo:4.2.2-bionic
```

**Le conteneur de l'application sera lancé en spécifiant le réseau**

Dans *application.properties* du projet spring boot, il faut rajouter l'uri de la base de donnée dont le moteur se trouve dans un autre conteneur.

```
spring.data.mongodb.uri=mongodb://dbcontainer:27017/<dbname>
```

### Installation Docker-Compose

[Documentation officielle - Docker](https://docs.docker.com/compose/install/)

```bash
# Installation pour Linux

sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose
```