![logo BYT](./images/byt_logo_landscape.png)

# `PROJET BUY YOUR TICKET`

L'idée de ce projet c'est de proposer une plateforme de billetterie en ligne pour l'événementiel avec la particularité d'effectuer les paiements via les portefeuilles électroniques des opérateurs mobiles.

`Mobile money` et `Mobile banking` sont les appellations couramment utilisées pour designer ces portefeuilles ratachés aux opérateurs mobiles.

## Flux des opérations

![workflow BYT](./images/byt_workflow.png)

1. Un organisateur d'événements (vendeur) crée son événement sur la plateforme BYT
2. Achat d'un ticket événementiel par un utilisateur lambda (client)
3. Traitement de l'opération monétaire par l'opérateur mobile. Voici un exemple d'informations que BYT peut communiquer à l'opérateur mobile :
    * numéro de téléphone du client (à débiter);
    * numéro de téléphone du vendeur (à créditer);
    * montant de la transaction.
4. L'opérateur débite le compte du client (si cet opération est possible)
5. Le compte du vendeur sera crédité si l'opération précédente a réussi. Cette dernière peut échouer par exemple dans le cas où le numéro de téléphone du client n'existe pas ou bien si son crédit est insuffisant pour faire un achat de ticket
6. L'opérateur retourne à BYT le statut de la transaction : **SUCCÈS** ou **ÉCHEC**
7. Notification sur le statut de la transaction
8. Notification sur le statut de la transaction

Sur l'ensemble des transactions, les comptes de BYT seront crédités selon un pourcentage négocié avec les vendeurs (organisateurs d'événements).

## Documentation Technique

Stack technique :

* Base de données
    * `MongoDB`
* Back-End
    * `Java Spring Boot`
* Front-End
    * `React JS`
* Conteneurisation
    * `Docker`

Le module de gestion des utilisateurs est séparé du reste des services.

- Gestion des utilisateur (application spring boot) : [springboot.auth.bytpayment.com](https://gitlab.com/buy-your-ticket/springboot.auth.bytpayment.com)
- Gestion des différents services de la plateforme (application spring boot) : [springboot.services.bytpayment.com](https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com)
- Code source de l'application full-front [BYT PAYMENT](https://demo.bytpayment.com) : [byt-react](https://github.com/dndayishima/byt-react/tree/master)

La documentation complète des APIs `BYT` est fournie dans le [README-DEV.md](https://gitlab.com/buy-your-ticket/documentation/-/blob/master/README-DEV.md). Celle-ci sera remplacée plus tard par une documentation dynamique des APIs avec l'outil [SWAGGER](https://swagger.io/).

### Comment tester la plateforme en localhost

Dans cette section, sont présentées les étapes à suivre pour exécuter les 2 applications spring boot `springboot.auth.bytpayment.com` et `springboot.services.bytpayment.com`, qui sont respectivement l'*application de gestion des utilisateurs* et l'*application des autres services de BYT*.

Le test de l'application *Front-End* est documenté sur ce repository github [byt-react](https://github.com/dndayishima/byt-react/tree/master).

Les différentes applications peuvent être executées dans des conteneurs `Docker` comme c'est le cas pour l'application déployée sur [https://demo.bytpayment.com](https://demo.bytpayment.com).
Sur ce README.md, seul `MongoBD` sera exécutée dans un conteneur `Docker`. Si Docker n'est pas installé sur votre poste, vous pouvez effectuer l'installation telle qu'elle est décrite dans la documentation officielle [docker](https://docs.docker.com/get-docker/).

Java 8 pré-requis :

```bash
sudo apt-get update
sudo apt-get install openjdk-8-jdk # Version minimale requise de java et javac
java -version
javac -version
```

Bases de données

```bash
# création et lancement d'un conteneur MongoBD
# pour les bases de données avec un volume
# ici nous utilisons l'image officielle Docker avec le tag 4.2.2-bionic
# mais il est tout à fait possible d'utiliser un autre tag d'une image
# beaucoup plus légère
docker run --name databases -v $HOME/.db:/data/db -p 27017:27017 -d mongo:4.2.2-bionic
```

Gestion des utilisateurs

```bash
git clone https://gitlab.com/buy-your-ticket/springboot.auth.bytpayment.com.git
cd springboot.auth.bytpayment.com
./mvnw spring-boot:run
```

Services BYT BAYMENT

```bash
git clone https://gitlab.com/buy-your-ticket/springboot.services.bytpayment.com.git
cd springboot.services.bytpayment.com
./mvnw spring-boot:run
```

### Simulation d'un achat de ticket

Nous distinguons plusieurs types d'utilisateurs en fonction de leurs `ROLE` (voir l'enumération [Role.java](https://gitlab.com/buy-your-ticket/springboot.auth.bytpayment.com/-/blob/master/src/main/java/com/bytpayment/auth/enums/Role.java)) :

- `USER` : utilisateur standard sans privilèges particuliers. Celui-ci consulte les évènements et peut acheter des tickets
- `SELLER` : utilisateur ayant un droit de création (publication) d'un évènement. Il peut également faire des achats de tickets pour des évènements ne lui appartenant pas
- `TECH` : utilisateur avec tous les droits d'un `SELLER` + quelques droits dont dispose uniquement un administrateur
- `ADMIN` : utilisateur ayant tous les droits, y compris celui d'administrer la plateforme

Nous allons créer 3 utilisateurs de types différents, [voir la requête de création d'un utilisateur](https://gitlab.com/buy-your-ticket/documentation/-/blob/master/README-DEV.md#create-ou-register) :

**Création de l'`utilisateur1`**

![utilisateur1_register](./images/utilisateur1_register.png)

Résultat de la requête de création de l'`utilisateur1`

![utilisateur1_register_result](./images/utilisateur2_register_result.png)

**`utilisateur2`**

![utilisateur2_register](./images/utilisateur2_register.png)

![utilisateur2_register_result](./images/utilisateur2_register_result.png)

**`utilisateur3`**

![utilisateur3_register](./images/utilisateur3_register.png)

![utilisateur3_register_result](./images/utilisateur3_register_result.png)

L'`utilisateur2` disposant d'un droit pour la création d'un évènement peut se connecter ([voir la requête de login d'un utilisateur](https://gitlab.com/buy-your-ticket/documentation/-/blob/master/README-DEV.md#login)) et la clé `JWT` reçue au login sera utilisée dans la [requête de création d'un évènement](https://gitlab.com/buy-your-ticket/documentation/-/blob/master/README-DEV.md#create).

![utilisateur2_login](./images/utilisateur2_login.png)

![utilisateur2_login_result](./images/utilisateur2_login_result.png)

**Création d'un évènement par l'`utilisateur2`**

![event_creation](./images/event_creation.png)

![event_creation_result](./images/event_creation_result.png)

Nous allons utiliser le compte de l'`utilisateur3` pour créer un `code` qui sera utilisé par la suite pour obtenir (créer) un ticket. On peut voir ce code comme étant celui qui est reçu par SMS (par exemple) après un paiement. Pour cet exemple, il s'agit d'une simulation, donc pas de paiement à effectuer. 

L'`utilisateur3` se connectera de la même façon que l'`utilisateur2`, la clé JWT reçue après le login sera utilisée dans la requête de création d'un `code`. [Voir la requête de création d'un code](https://gitlab.com/buy-your-ticket/documentation/-/blob/master/README-DEV.md#create-1).

**Création d'un code par l'`utilisateur3`**

![code_creation](./images/code_creation.png)

![code_creation_result](./images/code_creation_result.png)

Avec ce code qui vient d'être créé, l'`utilisateur1` peut obtenir un ticket. Il utilisera la valeur `code.code` dans la requête de [création d'un ticket](https://gitlab.com/buy-your-ticket/documentation/-/blob/master/README-DEV.md#create_by_code), sans oublier que l'utilisateur1 doit être authentifié.

Le login de l'utilisateur1 se fera de la même manière que les 2 précédents, ici nous montrons directement la requête et le résultat de création d'un ticket.

**Création d'un ticket par l'`utilisateur1`**

![ticket_creation](./images/ticket_creation.png)

![ticket_creation_result](./images/ticket_creation_result.png)

Le code utilisé pour la création d'un ticket ne peut plus être utilisé.